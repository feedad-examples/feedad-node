/**
 * Loads the FeedAd SDK into the given window.
 * Once the SDK was loaded you can request ads following the instructions on: {@link https://docs.feedad.com/web/installation/setup/index.html}
 *
 * @param [win] the window to load the FeedAd SDK in. Defaults to the global window object.
 * @return a Promise that resolves with the FeedAd SDK.
 */
export function loadFeedAd(win: Window = window): Promise<Sdk> {
	return new Promise<Sdk>(((resolve, reject) => {
		const script = win.document.createElement("script");
		const target = win.document.head || win.document.body;
		target.appendChild(script);
		script.async = true;
		script.onerror = () => reject(new Error("error loading FeedAd SDK"));
		script.onload = () => resolve((win as any).feedad as Sdk);
		script.src = "https://web.feedad.com/sdk/feedad-latest.js";
	}));
}

/**
 * Sdk initialization options
 */
export interface SdkOptions {
	/**
	 * if the sdk is included within a hybrid app
	 */
	hybrid_app?: boolean;
	/**
	 * the device platform of the app
	 */
	hybrid_platform?: string;
	/**
	 * the bundle id or package name of a hybrid app
	 */
	bundle_id?: string;
	/**
	 * the advertising ID of the user
	 */
	advertising_id?: string;
	/**
	 * the name of the app
	 */
	app_name?: string;
	/**
	 * for hybrid apps: if ad tracking is limited by the user
	 */
	limit_ad_tracking?: boolean;
	/**
	 * If the SDK should wait for TCF or GPP consent to be available.
	 * Only use this flag if the SDK can initialize before a CMP gives consent.
	 */
	wait_for_consent?: boolean;
}

/**
 * Options to pass to an ad request
 */
export interface AdRequestOptions {

	/**
	 * The type of the current internet connection.
	 */
	connectionType?: "unknown" | "wifi" | "2g" | "3g" | "4g";

	/**
	 * If the loading indicator should be shown when an ad is loading. Default: true.
	 */
	showLoadingIndicator?: boolean;
}

/**
 * Error codes for the public error listener
 */
export interface ErrorCode {
	/**
	 * There was an error while communicating with FeedAd services.
	 */
	SDK: 1,

	/**
	 * The error was caused by an invalid configuration of the FeedAd SDK.<br>
	 * Please check the documentation on how to configure the SDK.
	 */
	CONFIG: 2,

	/**
	 * An error was thrown while playing the media file.<br>
	 * E.g. when the device does not support the media file codecs.
	 */
	PLAYBACK: 3,

	/**
	 * An error was thrown because the URL of a media file is invalid.
	 */
	RESOURCE: 4,

	/**
	 * The request of the VAST file of the ad network failed.
	 */
	AD_NETWORK: 5,

	/**
	 * There is trouble with the internet connection.
	 */
	CONNECTION: 6,

	/**
	 * There are no ads available.
	 */
	NO_FILL: 7,

	/**
	 * There was an error while parsing the VAST file.
	 */
	PARSER: 8,

	/**
	 * The user did not give consent to use the FeedAd SDK.
	 */
	CONSENT: 9
}

/**
 * Setters for SDK-wide custom parameters
 */
export interface CustomParameters {

	/**
	 * Set the user's age in years. Use null to reset.
	 * @param {number} age
	 */
	userAge(age: number | null): void;

	/**
	 * Set the user id of your authentication system. Use null to reset.
	 * @param {string} id
	 */
	userId(id: string | null): void;

	/**
	 * Set the gender of the user. Use null to reset.
	 * @param {"m" | "f" | null} gender
	 */
	userGender(gender: "m" | "f" | null): void;

	/**
	 * Sets or deletes a new custom parameter for all placements
	 * @param {string} key - the parameter key
	 * @param {string | null} value - the parameter value or null to delete
	 */
	global(key: string, value: string | null): void;

	/**
	 * Sets or deletes a custom parameter for a specific placement
	 * @param {string} placementId - the placement id to set the params for
	 * @param {string} key - the parameter key
	 * @param {string | null} value - the parameter value or null to delete
	 */
	forPlacementId(placementId: string, key: string, value: string | null): void;
}

/**
 * Class explaining SDK errors
 */
abstract class FeedAdError {

	/**
	 * Constant to map error codes to readable names
	 */
	static ERROR_CODE: ErrorCode;

	/**
	 * @returns {number} the error code
	 */
	abstract readonly errorCode: number;

	/**
	 * @returns {string} the error message
	 */
	abstract readonly errorMessage: string;
}

/**
 * Response class for ad requests
 */
export interface AdResponse {

	/**
	 * @deprecated Use {@link createAdContainer} instead
	 * @return {string}
	 */
	element: string;

	/**
	 * Returns the promise that resolves when all ads have been completed
	 * @return {Promise}
	 */
	promise: Promise<void>;

	/**
	 * Creates a new ad container element to be used inside your DOM.
	 * @return {HTMLElement}
	 */
	createAdContainer(): HTMLElement;

	/**
	 * Removes an ad container previously created by {@link createAdContainer}.
	 * This will only remove the container from the FeedAd logic. You'll have to remove it from the DOM yourself.
	 * @param {HTMLElement} element - the element to remove
	 */
	removeAdContainer(element: HTMLElement): void;

	/**
	 * Updates the ad options of the current ad response.
	 * Please note, that an updated connectionType will be applied to the next ad after the current one finishes.
	 * @param options - the new options
	 */
	updateOptions(options: AdRequestOptions): void;
}

export interface Sdk {

	/**
	 * Checks if the current browser supports the FeedAd SDK.
	 * @returns {boolean} if the SDK is supported
	 */
	isSupported(): boolean;

	/**
	 * Initialize the SDK
	 * @param {string} clientToken - your client token
	 * @param {SdkOptions} options - additional options
	 * @return {Promise}
	 */
	init(clientToken: string, options?: SdkOptions): Promise<void>;

	/**
	 * Starts a FeedAd ad request.
	 * @param {string} placementId - the placement to request
	 * @param {AdRequestOptions} options - additional request options
	 * @return {Promise<AdResponse>} a promise that resolves with the ad response and rejects when no ad was found
	 */
	requestAd(placementId: string, options?: AdRequestOptions): Promise<AdResponse>;

	/**
	 * Starts a StandaloneAd ad request.
	 * @param {string} placementId - the placement to request
	 * @param {AdRequestOptions} options - additional request options
	 * @return {Promise<AdResponse>} a promise that resolves with the ad response and rejects when no ad was found
	 */
	requestStandaloneAd(placementId: string, options?: AdRequestOptions): Promise<AdResponse>;

	/**
	 * Requests a FeedAd with a timeout.
	 * The returned promise will only resolve when an ad was found within the given time.
	 * Otherwise it will reject.
	 *
	 * @param placementId - the placement to request
	 * @param timeoutMs - the timeout in milliseconds
	 * @returns a promise that resolves when an ad was found within the timeout.
	 */
	requestTargeting(placementId: string, timeoutMs: number): Promise<any>;

	/**
	 * Sends a custom event to the FeedAd API
	 * @param {string} label - the event label
	 * @param {number} revenue - the revenue this event generated, if any
	 * @param {string} currency - the ISO 4217 currency code if the revenue
	 */
	sendEvent(label: string, revenue?: number, currency?: string): void;

	customParameters: CustomParameters;

	FeedAdError: typeof FeedAdError;
}