# FeedAd Node.JS Loader

This library can be used to load the FeedAd advertisement SDK into an application.

- Visit [FeedAd.com](https://feedad.com) for general information about FeedAd.
- Visit [docs.FeedAd.com](https://docs.feedad.com) for information on how to integrate FeedAd into your site.

## Requirements

- DOM Context (this is not a Node.JS server-side library)
- Promises

## Example

```
import {loadFeedAd} from "feedad-node";

loadFeedAd().then(sdk => {
    sdk.init(/* ... */);
    sdk.requestAd(/* ... */);
    // check the documentation on how to use the sdk.
});
```